#!/bin/bash

echo "$PATH"
export PATH=/usr/local/bin:$PATH

### List off systems pre-compiled libraries
declare -a listL=(
	"bdb-6.2.23-darwin.17-x86_64.tar.gz"
	"boost-1.73.0-darwin.17-x86_64.tar.gz"
	"boost-headers-1.69-0-darwin.17-x86_64.tar.gz"
	"cairo-1.14.12-darwin.17-x86_64.tar.gz"
	"cairomm-1.11.2-darwin.17-x86_64.tar.gz"
	"ffi-3.3-darwin.17-x86_64.tar.gz"
	"fftw-3.3.8-darwin.17-x86_64.tar.gz"
	"fontconfig-2.13.1-darwin.17-x86_64.tar.gz"
	"freetype-2.10.0-darwin.17-x86_64.tar.gz"
	"freexl-1.0.6-darwin.17-x86_64.tar.gz"
	"gdal-3.1.1-darwin.17-x86_64.tar.gz"
	"geos-3.8.1-darwin.17-x86_64.tar.gz"
	"glpk-4.65-darwin.17-x86_64.tar.gz"
	"gmp-6.2.0-darwin.17-x86_64.tar.gz"
	"gsl-2.6-darwin.17-x86_64.tar.gz"
	"hdf5-1.12.0-darwin.17-x86_64.tar.gz"
	"icu-67.1-darwin.17-x86_64.tar.gz"
	"jpeg-9-darwin.17-x86_64.tar.gz"
	"libpng-1.6.37-darwin.17-x86_64.tar.gz"
	"libpq-9.6.9-darwin.17-x86_64.tar.gz"
	"libsndfile-1.0.28-darwin.17-x86_64.tar.gz"
	"libsodium-1.0.18-darwin.17-x86_64.tar.gz"
	"libwebp-1.1.0-darwin.17-x86_64.tar.gz"
	"mpfr-4.0.2-darwin.17-x86_64.tar.gz"
	"netcdf-4.7.4-darwin.17-x86_64.tar.gz"
	"openjpeg-2.3.1-darwin.17-x86_64.tar.gz"
	"openssl-1.1.1g-darwin.17-x86_64.tar.gz"
	"pcre2-10.34-darwin.17-x86_64.tar.gz"
	"pixman-0.38.4-darwin.17-x86_64.tar.gz"
	"pkgconfig-0.28-darwin.17-x86_64.tar.gz"
	"proj-6.3.1-darwin.17-x86_64.tar.gz"
	"protobuf-3.11.4-darwin.17-x86_64.tar.gz"
	"qpdf-9.1.1-darwin.17-x86_64.tar.gz"
	"QuantLib-1.18-darwin.17-x86_64.tar.gz"
	"readline-5.2.14-darwin.17-x86_64.tar.gz"
	"sigc++-4.2.1-darwin.17-x86_64.tar.gz"
	"sqlite3-3.32.3-darwin.17-x86_64.tar.gz"
	"szip-2.1.1-darwin.17-x86_64.tar.gz"
	"texinfo-6.7-darwin.17-x86_64.tar.gz"
	"tiff-4.1.0-darwin.17-x86_64.tar.gz"
	"udunits-2.2.24-darwin.17-x86_64.tar.gz"
	"xml2-2.9.10-darwin.17-x86_64.tar.gz"
	"xz-5.2.4-darwin.17-x86_64.tar.gz"
	"zeromq-4.3.2-darwin.17-x86_64.tar.gz"
	"zlib-system-stub.tar.gz"
)

for i in "${listL[@]}"
do
	echo "Download and install $i"
	curl -LO "http://mac.R-project.org/libs-4/$i"
	# bsdtar return error... why true is return
	sudo tar fvxz "$i" -C / | true
	rm $i
done

echo "Installing R packages"

echo "dir.create(Sys.getenv('R_LIBS_USER'), recursive=TRUE); \
  .libPaths( c(Sys.getenv('R_LIBS_USER'), .libPaths()) ) ; \
  install.packages(c( \
  'deSolve', \
  'sp', \
  'rgeos', \
  'fields', \
  'MASS', \
  'Matrix', \
  'deldir', \
  'pracma', \
  'raster', \
  'fftwtools', \
  'mvtnorm', \
  'rgdal', \
  'sf', \
  'splancs', \
  'knitr', \
  'rmarkdown', \
  'testthat', \
  'Rcpp', \
  'RcppArmadillo', \
  'fasterize', \
  'roxygen2', \
  'shiny', \
  'htmltools', \
  'shinydashboard', \
  'ggplot2', \
  'dplyr', \
  'dbplyr', \
  'DT', \
  'magrittr', \
  'devtools', \
  'shinyjs', \
  'stringi', \
  'stringr', \
  'jsonlite', \
  'data.table', \
  'htmlwidgets', \
  'RSQLite', \
  'BiocManager', \
  'foreach', \
  'doParallel', \
  'mvtnorm', \
  'mapdata', \
  'proj4', \
  'gstat', \
  'automap', \
  'RCALI', \
  'rgenoud', \
  'purrr' \
  ), \
  dependencies=TRUE, \
  repos='https://cran.biotools.fr')" > /tmp/install.R 

Rscript /tmp/install.R

Rscript -e "BiocManager::install(c('Biostrings', ask=FALSE))"
Rscript -e "update.packages(ask=FALSE, repos='https://cran.biotools.fr')"

